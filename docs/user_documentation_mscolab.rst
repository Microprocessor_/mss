User Documentation - Mscolab(development version)
=================================================
Mscolab server has to be manually installed, the instructions can be found at https://mss.readthedocs.io/en/stable/development.html 
Once mss is installed, mscolab server can be started by the command.
python mslib/mscolab/server.py
Mscolab UI will be in the future, the new main UI. For now, to get also users’ experience included, we add it as a new menu in the tool-bar. We evolve the new UI  by these inputs of more users and will deprecate in this process the old one.
Once the server is set, mslib/mscolab/conf.py has to be updated. A description of constants can be found in the file.
To start mscolab from UI,  select `mscolab` option in msui menu.

Feature List
-------------
**User-Based**
- Users can sign up, and log in.

**Project-Based**
-Once logged in, a project can be created.
-The project list can be found in mscolab’s main window, along with permissions
-One has to activate the project, and options to open view windows/project window would show. ( Project window has options to chat, checkout to an older change, add or remove collaborators)
-While creating a project, one can select a file to work with.

permissions
-------------
There are 4 categories of users.

- **Creator** 
The creator is a user who creates the project, they have all the rights which ‘admins’ have. Additionally, they can make administrators and revoke the administrators’ status.

- **Admins**
Administrators can enable autosave(more on this later in this documentation). 
They can change categories of collaborators and viewers. They have all the capabilities of a collaborator.

- **Collaborators**
They can chat in the room, and make changes to the collaborative project.

- **Viewer**
They can only view the changes in waypoints and chat in a room.

All the changes to users’ permission are realtime.

NOTE:
------
-Autosave mode has to be enabled at this stage. Only admins/creators can enable autosave. If enabled, all the changes are synced across all instances of mscolab opened by users. Else there’d be conflicted files.

-However, all the changes are stored as VCS commits, so the project can be reverted to any past state safely.

For further details, see :ref:`mscolab`